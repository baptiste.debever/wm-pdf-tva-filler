const hummus = require('hummus');
const PDFDigitalForm = require('./pdf-digital-form');
const fs = require('fs');
const fillPdf = require('fill-pdf');
const path = require('path');
const data = require('./data.json')


const readFields = (templatePath = './fillable-tva.pdf') => {
	const pdfParser = hummus.createReader(templatePath);
	const form = new PDFDigitalForm(pdfParser);

	if (!form.hasForm()) return console.error('No PDF fields found');
	console.log(form.fields);

	return form.fields;
}

const utf8_from_str = (s) => {
	for (var i = 0, enc = encodeURIComponent(s), a = []; i < enc.length;) {
		if (enc[i] === '%') {
			a.push(parseInt(enc.substr(i + 1, 2), 16))
			i += 3
		} else {
			a.push(enc.charCodeAt(i++))
		}
	}
	return a
}

const utf8_to_str = (a) => {
	for (var i = 0, s = ''; i < a.length; i++) {
		var h = a[i].toString(16)
		if (h.length < 2) h = '0' + h
		s += '%' + h
	}
	return decodeURIComponent(s)
}

const filler = () => {
	const pdfTemplatePath = path.resolve(__dirname + '/fillable-tva.pdf');

	//const fields = readFields();

	for (let i = 0; i < data.length; i++) {
		console.log(data[i]);

		for (const key in data[i]) {
			const element = data[i][key];
			data[i][key] = utf8_from_str(element);
			data[i][key] = utf8_to_str(data[i][key]).normalize("NFD").replace(/[\u0300-\u036f]/g, "");

		}
		fillPdf.generatePdf(data[i], pdfTemplatePath, (err, output) => {
	    	if (!err)
	    		console.error(err);

	    	fs.writeFileSync(`./pdf/attestation-tva-10-${data[i].id}.pdf`, output);
  		});
	}

}


filler();