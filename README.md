This script allows you to automatically generate PDF files on your local machine based on a list of `copropriétés`.  

## How to use?

1. Check that the PDF template is up to date: fillable-tva.pdf. Otherwise, replace the file using the same filename.
2. Make sure your Excel file follows the same pattern as the template provided: 
- a1: Last name of the Gestionnaire
- a2: First name of the Gestionnaire
- a3, a4, a5: Address of the syndic
3. Convert your PDF file to a JSON format using an online converter, such as: https://anyconv.com/pdf-to-json-converter/
4. Save the generated JSON file at the root of the folder using the filename `data.json` 
5. Run the following command: `node index.js`

That's it! You should have the PDF files generated into the `/pdf` folder. 